class Customer < ActiveRecord::Base

  has_many :sales

  def number_to_currency(number, options = {})
    return unless number
    options = escape_unsafe_options(options.symbolize_keys)

    wrap_with_output_safety_handling(number, options.delete(:raise)) {
      ActiveSupport::NumberHelper.number_to_currency(number, options)
    }
    end

  def total_sales
    self.sales.map(&:amount_with_tax).sum
  end

end
